package org.com.ws.range.zipcode

import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks.{break, breakable}

object MainMinRangeScala {

  //def getRanges(): ListBuffer[Range] = ListBuffer(94133 to 94133, 94200 to 94299, 94600 to 94699)

  // output :  [94133,94133] [94200,94299] [94600,94699]

  def getRanges(): ListBuffer[Range] = ListBuffer(94133 to 94133, 94200 to 94299, 94226 to 94399)

  //output :  [94133,94133] [94200,94399]

  def sortValues(list: List[Range]) = list.sortBy(_.head)

  def main(args: Array[String]): Unit = {

    /*println("Program to min ranges from multiple Zip codes");*/
    println(s"Input   : \n   ${getRanges}")
    println(s"output  : \n   ${getMinRanges(getRanges)}")
    /*println("end")*/

  }

  def getMinRanges(list: ListBuffer[Range]): List[Range] = {
    var loopcount = 0;
    breakable {
      for (a <- list; b <- list) {
        if (a != b) {
          if (a.intersect(b).length > 0) {
            val op = (a union b distinct).sorted
            val finalList = op.head to op.last
            list.remove(list.indexOf(a))
            list.remove(list.indexOf(b))
            list += finalList
            loopcount = 1
            break()
          }
        }
      }
    }
    if (loopcount == 1) {
      getMinRanges(list)
    }
    else {

       sortValues(list.toList)

    }
  }
}
