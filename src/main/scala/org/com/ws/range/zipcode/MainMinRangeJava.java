package org.com.ws.range.zipcode;

import java.time.temporal.ValueRange;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MainMinRangeJava {

    public static void main(String args[]) {

        System.out.println("Program : Min ranges ");

        List<ValueRange> valueRange = getRanges();
        System.out.println("Input value :  \n " + getRanges());
        System.out.println("Output value : \n" + getMinZipCode(valueRange));
    }

    public static List getRanges() {
        //input 1 : [94133,94133] [94200,94299] [94600,94699]
        // input 2 : [94133,94133] [94200,94299] [94226,94399]
        ValueRange first = ValueRange.of(94133, 94133);
        ValueRange second = ValueRange.of(94200, 94299);
        ValueRange third = ValueRange.of(94226, 94399);

        List<ValueRange> ranges = new LinkedList<>();
        ranges.add(first);
        ranges.add(second);
        ranges.add(third);

        return ranges;

    }

    public static List getMinZipCode(List list) {
        int loopcount = 0;
        System.out.println("get Min ZipCode");

        do {
            outerloop:
            for (int i = 0; i < list.size(); i++) {
                loopcount = 0;
                for (int j = 0; j < list.size(); j++) {
                    if (i != j) {
                        ValueRange a = (ValueRange) list.get(i);
                        ValueRange b = (ValueRange) list.get(j);
                        boolean minFlag = a.isValidValue(b.getMaximum());
                        boolean maxFlag = a.isValidValue(b.getMinimum());
                        loopcount = 0;
                        if (minFlag || maxFlag) {
                            ArrayList finalValue = new ArrayList();
                            finalValue.add((int) a.getMaximum());
                            finalValue.add((int) a.getMinimum());
                            finalValue.add((int) b.getMinimum());
                            finalValue.add((int) b.getMaximum());
                            ValueRange op = ValueRange.of((Integer) Collections.min(finalValue), (Integer) Collections.max(finalValue));
                            list.remove(a);
                            list.remove(b);
                            list.add(op);
                            loopcount = 1;
                            break outerloop;
                        }
                    }
                }

            }
        } while (loopcount == 1);

        return list;

    }
}
